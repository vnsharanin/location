package com.shar.dao;

import com.shar.model.Role;
import java.util.List;

public interface IRoleDAO {
        List<Role> listRoles();
	Role findById(int id);
	void saveRole(Role role);   
}
