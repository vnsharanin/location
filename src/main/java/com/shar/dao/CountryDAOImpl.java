package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.Country;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("ICountryDao")
public class CountryDaoImpl implements ICountryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Country> listCountries() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from Country").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get countries from DB", ex);
        }
    }

    public void saveCountry(Country country) {
        sessionFactory.getCurrentSession().saveOrUpdate(country);
    }

    @Override
    public void deleteCountry(int id) {
        Country country = (Country) sessionFactory.getCurrentSession().load(
                Country.class, id);
        if (null != country) {
            sessionFactory.getCurrentSession().delete(country);
        }
    }

    @Override
    public Country getCountry(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Country where id = :id");
        query.setLong("id", id);
        return (Country) query.uniqueResult();
    }
}
