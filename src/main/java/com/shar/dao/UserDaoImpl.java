package com.shar.dao;

import com.shar.model.Role;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.shar.model.User;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public User findById(int id) {
        return getByKey(id); //to AbstractDao
    }

    public User findByEmail(String email) {
        Criteria crit = createEntityCriteria(); //to AbstractDao
        crit.add(Restrictions.eq("email", email));
        return (User) crit.uniqueResult();
    }

    private Role getRoleByAlias(String role) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Roles where role = :role");
        query.setString("role", role);
        return (Role) query.uniqueResult();
    }

    public void saveUser(User user) {
        try {
            user.setState("Active");
            ShaPasswordEncoder sha = new ShaPasswordEncoder();
            user.setPassword(sha.encodePassword(user.getPassword(),""));
            /*Set<Role> roles = new HashSet<Role>();
            roles.add(getRoleByAlias("USER"));
            user.setRole(roles);
            user.setSsoId(user.getEmail());*/
            sessionFactory.getCurrentSession().saveOrUpdate(user);
        } catch (Exception ex) {
            throw new RuntimeException("Can't save:\n" + user.getEmail()+"\n"+user.getPassword(), ex);
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

}
