package com.shar.dao;

import com.shar.model.Role;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("IRoleDao")
public class RoleDAOImpl implements IRoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Role> listRoles() {
        return sessionFactory.getCurrentSession().createQuery("from Role").list();
    }

    public Role findById(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Roles where id = :id");
        query.setInteger("id", id);
        return (Role) query.uniqueResult();
    }

    public void saveRole(Role role) {
        sessionFactory.getCurrentSession().saveOrUpdate(role);
    }
}
