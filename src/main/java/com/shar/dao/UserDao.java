package com.shar.dao;

import com.shar.model.User;
import java.util.List;

public interface UserDao {
        List<User> listUsers();
	User findById(int id);
	void saveUser(User user);
	User findByEmail(String email);	
}

