package com.shar.dao;

import com.shar.model.Region;
import java.util.List;

public interface IRegionDAO {
    List<Region> listRegionsByCountry(int country);
}
