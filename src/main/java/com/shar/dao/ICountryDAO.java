package com.shar.dao;

import com.shar.model.Country;
import java.util.List;

public interface ICountryDAO {
    List<Country> listCountries();
    void saveCountry(Country country);
    void deleteCountry(int id);
    Country getCountry(int id);
}