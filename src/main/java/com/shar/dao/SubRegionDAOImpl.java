package com.shar.dao;

import com.shar.model.SubRegion;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("ISubRegionDAO")
public class SubRegionDAOImpl implements ISubRegionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<SubRegion> listSubRegionsByRegion(int region) {
        return sessionFactory.getCurrentSession().createCriteria(SubRegion.class).add(Restrictions.eq("region.id", region)).list();
    }
}
