package com.shar.dao;

import com.shar.model.Region;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("IRegionDAO")
public class RegionDAOImpl implements IRegionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<Region> listRegionsByCountry(int country) {
        return sessionFactory.getCurrentSession().createCriteria(Region.class)
                .add(Restrictions.eq("country.id", country))
                .list();
    }
}
