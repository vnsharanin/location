package com.shar.dao;

import com.shar.model.SubRegion;
import java.util.List;

public interface ISubRegionDAO {
    List<SubRegion> listSubRegionsByRegion(int region); 
}