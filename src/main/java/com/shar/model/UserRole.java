package com.shar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//USER ADMIN DBA MANAGER
@Entity
@Table(name = "users_roles")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /*
     @Id
     @Column(name = "user_id", length = 20, unique = true, nullable = false)
     private int userId;

     @Id
     @Column(name = "role_id", length = 20, unique = true, nullable = false)
     private int roleId;*/

    @JoinColumn(name = "user_id")
    @ManyToOne
    private User user;

    @JoinColumn(name = "role_id")
    @ManyToOne
    private Role role;

    /* public UserRole(){};
     public UserRole(int userid, int roleId){
     this.userId = userId;
     this.roleId = roleId;
     }*/
    /*
     public int getUserId() {
     return userId;
     }
     public void setUserId(int userId) {
     this.userId = userId;
     }
     public int getRoleId() {
     return roleId;
     }
     public void setRoleId(int roleId) {
     this.roleId = roleId;
     }*/
    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
