package com.shar.controller;

import com.shar.service.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CountryController {

    @Autowired
    private ICountryService countryService;

    @RequestMapping("/country")
    public String listContacts(ModelMap model) {
        model.addAttribute("countries", countryService.listCountries());
        return "location";
    }
}
