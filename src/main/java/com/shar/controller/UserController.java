package com.shar.controller;

import com.shar.model.User;
import com.shar.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/register/new"}, method = RequestMethod.POST)
    public String addUser(@Valid User user, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "register";
        }
        userService.saveUser(user);
        return "redirect:/home";
    }

    @RequestMapping("/admin/users")
    public String listUsers(ModelMap model) {
        model.addAttribute("listOfUsers", userService.listUsers());
        return "users";
    }
}
