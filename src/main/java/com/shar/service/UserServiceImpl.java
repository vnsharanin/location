package com.shar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shar.dao.UserDao;
import com.shar.model.User;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao dao;

    public User findById(int id) {
        return dao.findById(id);
    }

    public User findByEmail(String email) {
        return dao.findByEmail(email);
    }

    public void saveUser(User user) {
        dao.saveUser(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers() {
        return dao.listUsers();
    }
}
