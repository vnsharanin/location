package com.shar.service;

import com.shar.model.User;
import java.util.List;

public interface UserService {
        List<User> listUsers();
	User findById(int id);
	void saveUser(User user);
	User findByEmail(String email);
}