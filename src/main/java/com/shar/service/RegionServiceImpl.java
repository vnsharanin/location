package com.shar.service;

import com.shar.model.Region;
import com.shar.dao.IRegionDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("IRegionService")
@Transactional
public class RegionServiceImpl implements IRegionService {

    @Autowired
    private IRegionDAO regionDAO;

    @Override
    public List<Region> listRegionsByCountry(int country) {
        return regionDAO.listRegionsByCountry(country);
    }

}
