package com.shar.service;

import com.shar.model.Role;
import java.util.List;

public interface IRoleService {
        List<Role> listRoles();
	Role findById(int id);
	void saveRole(Role role);      
}
