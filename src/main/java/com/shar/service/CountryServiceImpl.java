package com.shar.service;

import com.shar.model.Country;
import com.shar.dao.ICountryDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ICountryService")
@Transactional
public class CountryServiceImpl implements ICountryService {
    @Autowired
    private ICountryDAO countryDAO;

    @Override
    public List<Country> listCountries() {
        return countryDAO.listCountries();
    }
    
    @Override
    public void saveCountry(Country country) {
        countryDAO.saveCountry(country);
    }

    @Override
    public void deleteCountry(int id) {
        countryDAO.deleteCountry(id);
    }

    @Override
    public Country getCountry(int id) {
        return countryDAO.getCountry(id);
    }
}
