package com.shar.service;

import com.shar.dao.IRoleDAO;
import com.shar.model.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("IRoleService")
@Transactional
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleDAO roleDAO;

    public List<Role> listRoles() {
        return roleDAO.listRoles();
    }

    public Role findById(int id) {
        return roleDAO.findById(id);
    }

    public void saveRole(Role role) {
        roleDAO.saveRole(role);
    }
}
