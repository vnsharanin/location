package com.shar.service;

import com.shar.model.SubRegion;
import com.shar.dao.ISubRegionDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service("ISubRegionService")
@Transactional
public class SubRegionServiceImpl implements ISubRegionService {
    @Autowired
    private ISubRegionDAO subRegionDAO;

    @Override
    public List<SubRegion> listSubRegionsByRegion(int region) {
        return subRegionDAO.listSubRegionsByRegion(region);
    }
}
