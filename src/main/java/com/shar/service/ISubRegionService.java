package com.shar.service;

import com.shar.model.SubRegion;
import java.util.List;

public interface ISubRegionService {
    List<SubRegion> listSubRegionsByRegion(int region);
}
