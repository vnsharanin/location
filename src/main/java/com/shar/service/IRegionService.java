package com.shar.service;

import com.shar.model.Region;
import java.util.List;

public interface IRegionService {
    List<Region> listRegionsByCountry(int country);
}
