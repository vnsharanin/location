package com.shar.service;

import com.shar.model.Country;
import java.util.List;

public interface ICountryService {
    List<Country> listCountries();
    void saveCountry(Country country);
    void deleteCountry(int id);
    Country getCountry(int id);
}
