<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>HelloWorld Login page</title>
    </head>

    <body>
        <sec:authorize access="!isAuthenticated()">
            <div>
                <c:url var="loginUrl" value="/login" />
                <form:form action="${loginUrl}" method="post">
                    <c:if test="${param.error != null}">
                        <div>
                            <p>Invalid username and password.</p>
                        </div>
                    </c:if>
                    <c:if test="${param.logout != null}">
                        <div>
                            <p>You have been logged out successfully.</p>
                        </div>
                    </c:if>
                    <div>
                        <input type="text" id="username" name="ssoId" placeholder="Enter Username" required>
                    </div>
                    <div>
                        <input type="password" id="password" name="password" placeholder="Enter Password" required>
                    </div>
                    <div>
                        <label><input type="checkbox" id="rememberme" name="remember-me"> Remember Me</label>  
                    </div>
                    <div>
                        <input type="submit" value="Log in">
                    </div>
                </form:form>
            </div>
        </sec:authorize>
    </body>
</html>