<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table>
    <tr>
        <td><form:input path="email" id="email" /></td>
        <td><form:errors path="email" cssclass="error"></form:errors></td>
        </tr>
        <tr>
            <td><form:password path="password" id="password"/>
            <input type="button" value="Show password" onclick="javascript:showPassword();"/>
        </td>

        <td><form:errors path="password" cssclass="error"></form:errors></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Register"/>
        </td>
    </tr>
</table>
<script type="text/javascript">
    function showPassword() {//Will do it with JQuery!
        var password = document.getElementById("password");
        if (password.getAttribute("type") == "password") {
            password.setAttribute('type', 'input');
        } else {
            password.setAttribute('type', 'password');
        }
    }
</script>