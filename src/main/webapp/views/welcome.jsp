<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>HelloWorld page</title>
    </head>
    <body>
        Greeting : ${greeting}
        This is a welcome page.

        <div>
            <sec:authorize access="!isAuthenticated()">
                <p><a href="<c:url value="/login" />">Sign In</a></p>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <p>Your email is: <sec:authentication property="principal.username" /></p>
                <p><a href="<c:url value="/logout" />">Logout</a></p>
            </sec:authorize>
        </div>
</body>
</html>