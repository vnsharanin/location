<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Users</title>
    </head>
    <body>
        
        <h2>User List</h2>

        <c:if test="${!empty listOfUsers}">
            <table class="data">
                <tr>
                    <th>email</th>
                    <th>password</th>
                    <th>&nbsp;</th>
                </tr>
                <c:forEach items="${listOfUsers}" var="user">
                    <tr>
                        <td>${user.email}</td>
                        <td>${user.password}</td>
                        <td><a href="${pageContext.request.contextPath}/admin/user/edit/${user.id}"></a></td>
                        <td><a href="${pageContext.request.contextPath}/admin/user/delete/${user.id}"></a></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>

    </body>
</html>